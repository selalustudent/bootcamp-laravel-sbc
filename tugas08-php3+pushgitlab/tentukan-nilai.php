<?php
function tentukan_nilai($number)
{
    if (85<=$number && $number<=100){echo "Sangat Baik <br>";}
    elseif (70<=$number && $number<85){echo "Baik <br>";}
    elseif (60<=$number && $number<70){echo "Cukup <br>";}
    elseif ($number<60){echo "Kurang <br>";}
    return;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>